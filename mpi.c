// C++ implementation of Radix Sort
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <mpi.h>


// Driver Code
int main(int argc, char *argv[])
{
    FILE* fp;
    int n;
    int num_proc, rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        fp = fopen("input3.txt", "r");
        fscanf(fp, "%d", &n);
    }

    MPI_Bcast(&n, 1, MPI_LONG, 0, MPI_COMM_WORLD);

    int orignal_n = n;
    while (n % num_proc != 0) {
        n++;
    }

    int* arr = (int*) malloc(sizeof(int) * n);
    int* zeroQueue = (int*) malloc(sizeof(int) * n);
    int* onesQueue = (int*) malloc(sizeof(int) * n);

    int chunkSize = n / num_proc;


    printf("Greetings from proc %d chunk size = %d \n", rank, chunkSize);

    int* chunkArr = (int*) malloc(sizeof(int) * chunkSize);
    int* chunkZeros = (int*) malloc(sizeof(int) * chunkSize);
    int* chunkOnes = (int*) malloc(sizeof(int) * chunkSize);

    if (rank == 0) {
        for (int i = 0; i < orignal_n; i++) {
            fscanf(fp, "%d", &arr[i]);
        }
        for (int i = orignal_n; i < n; i++) {
            arr[i] = INT_MAX;
        }
        fclose(fp);
    }

    for (int i = 0; i < 32; i++) {
        int mask = (1 << i);

        MPI_Scatter(arr, chunkSize, MPI_INT, chunkArr, chunkSize, MPI_INT, 0, MPI_COMM_WORLD);

        int zeros = 0;
        int ones = 0;

        for (int j = 0; j < chunkSize; j++) {
            chunkZeros[j] = -1;
            chunkOnes[j] = -1;
        }

        for (int j = 0; j < chunkSize; j++) {
            if ((chunkArr[j] & mask) != 0)
            {
                chunkOnes[ones] = chunkArr[j];
                ones++;
            }
            else
            {
                chunkZeros[zeros] = chunkArr[j];
                zeros++;
            }
        }

        MPI_Gather(chunkZeros, chunkSize, MPI_INT, zeroQueue, chunkSize, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Gather(chunkOnes, chunkSize, MPI_INT, onesQueue, chunkSize, MPI_INT, 0, MPI_COMM_WORLD);

        if (rank == 0) {
            int cnt = 0;

            for (int j = 0; j < num_proc; j++) {
                for (int k = 0; k < chunkSize; k++) {
                    int idx = j * chunkSize + k;
                    if (zeroQueue[idx] == -1) {
                        break;
                    }
                    arr[cnt++] = zeroQueue[idx];
                }
            }
            for (int j = 0; j < num_proc; j++) {
                for (int k = 0; k < chunkSize; k++) {
                    int idx = j * chunkSize + k;
                    if (onesQueue[idx] == -1) {
                        break;
                    }
                    arr[cnt++] = onesQueue[idx];
                }
            }
        }
    }

    if (rank == 0) {
        fp = fopen("output.txt", "w");
        for (int i = 0; i < orignal_n; i++) {
            fprintf(fp, "%d ", arr[i]);
        }
        fprintf(fp, "\n");
        fclose(fp);
    }

    free(zeroQueue);
    free(onesQueue);
    free(chunkArr);
    free(chunkZeros);
    free(chunkOnes);

    MPI_Finalize();

    return 0;
}
