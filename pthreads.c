// C++ implementation of Radix Sort
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <pthread.h>

int num_threads;
int n;
int mask;

int* arr;
int* zeroQueue;
int* onesQueue;

void solve(void* args) {
    int tid = *((int*)args);
    int ones = 0;
    int zeros = 0;

    int chunkSize = n / num_threads;
    if (n % num_threads) {
        chunkSize++;
    }
    int left = tid * chunkSize;
    int right = (tid + 1) * chunkSize;
    if (right > n) {
        right = n;
    }

    for (int i = left; i < right; i++) {
        if ((arr[i] & mask) != 0) {
            onesQueue[left + ones] = arr[i];
            ones++;
        } else {
            zeroQueue[left + zeros] = arr[i];
            zeros++;
        }
    }
}

// Driver Code
int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("Please insert the number of threads as an argument\n");
        exit(-1);
    }

    num_threads = atoi(argv[1]);
    pthread_t tds[num_threads];
    int thread_ids[num_threads];

    FILE* fp = fopen("input3.txt", "r");
    fscanf(fp, "%d", &n);

    arr = (int*) malloc(sizeof(int) * n);
    zeroQueue = (int*) malloc(sizeof(int) * n);
    onesQueue = (int*) malloc(sizeof(int) * n);

    for (int i = 0; i < n; i++) {
        fscanf(fp, "%d", &arr[i]);
    }
    fclose(fp);

    for (int i = 0; i < 32; i++) {
        mask = (1 << i);
        for (int j = 0; j < n; j++) {
            zeroQueue[j] = -1;
            onesQueue[j] = -1;
        }

        for (int j = 0; j < num_threads; j++) {
            thread_ids[j] = j;
            int ret = pthread_create(&tds[j], NULL, solve, &thread_ids[j]);
            if (ret != 0)
            {
                printf("Failed to create thread\n");
                exit(0);
            }
        }

        for (int j = 0; j < num_threads; j++) {
            pthread_join(tds[j], NULL);
        }

        int cnt = 0;
        for (int j = 0; j < num_threads; j++) {
            int chunkSize = n / num_threads;
            if (n % num_threads) {
                chunkSize++;
            }
            int left = j * chunkSize;
            int right = (j + 1) * chunkSize;
            if (right > n) {
                right = n;
            }

            for (int k = left; k < right && zeroQueue[k] != -1; k++) {
                arr[cnt++] = zeroQueue[k];
            }
        }
        for (int j = 0; j < num_threads; j++) {
            int chunkSize = n / num_threads;
            if (n % num_threads) {
                chunkSize++;
            }
            int left = j * chunkSize;
            int right = (j + 1) * chunkSize;
            if (right > n) {
                right = n;
            }

            for (int k = left; k < right && onesQueue[k] != -1; k++) {
                arr[cnt++] = onesQueue[k];
            }
        }

    }

    fp = fopen("output.txt", "w");
    for (int i = 0; i < n; i++) {
        fprintf(fp, "%d ", arr[i]);
    }
    fprintf(fp, "\n");
    fclose(fp);

    free(arr);
    free(zeroQueue);
    free(onesQueue);

    return 0;
}
