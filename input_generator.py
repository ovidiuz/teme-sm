import random

n = 200000000

f = open("input6.txt", "w")
f.write(str(n) + '\n')
# random.seed(1)
for i in range(n):
    value = random.randint(0, 2147483647) # 2147483647 = INT_MAX in limits.h
    f.write(str(value) + ' ')