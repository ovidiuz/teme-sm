// C++ implementation of Radix Sort
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <omp.h>

int num_threads;
int n;
int mask;

int* arr;
int* zeroQueue;
int* onesQueue;

double start_time;
double end_time;
double exec_time;

// Driver Code
int main(int argc, char *argv[])
{
    FILE* fp = fopen("input6.txt", "r");
    fscanf(fp, "%d", &n);

    arr = (int*) malloc(sizeof(int) * n);
    zeroQueue = (int*) malloc(sizeof(int) * n);
    onesQueue = (int*) malloc(sizeof(int) * n);

    for (int i = 0; i < n; i++) {
        fscanf(fp, "%d", &arr[i]);
    }
    fclose(fp);

    start_time = omp_get_wtime();
    for (int i = 0; i < 32; i++) {
        mask = (1 << i);
        int cnt = 0;

        #pragma omp parallel
        {
            int chunk_size = n / omp_get_num_threads();
            if (n % omp_get_num_threads()) {
                chunk_size++;
            }

            int tid = omp_get_thread_num();
            int left = tid * chunk_size;
            int right = (tid + 1) * chunk_size;
            if (right > n) {
                right = n;
            }
            for (int j = left; j < right; j++) {
                if ((arr[j] & mask) != 0) {
                    onesQueue[j] = arr[j];
                    zeroQueue[j] = -1;
                } else {
                    zeroQueue[j] = arr[j];
                    onesQueue[j] = -1;
                }
            }

        }

        for (int j = 0; j < n; j++) {
            if (zeroQueue[j] != -1) {
                arr[cnt] = zeroQueue[j];
                cnt++;
            }
        }
        for (int j = 0; j < n; j++) {
            if (onesQueue[j] != -1) {
                arr[cnt] = onesQueue[j];
                cnt++;
            }
        }
    }
    end_time = omp_get_wtime();
    exec_time = end_time - start_time;
    printf("Time elapsed = %f", exec_time);

    fp = fopen("output.txt", "w");
    for (int i = 0; i < n; i++) {
        fprintf(fp, "%d ", arr[i]);
    }
    fprintf(fp, "\n");
    fclose(fp);

    free(arr);
    free(zeroQueue);
    free(onesQueue);

    return 0;
}
